using System.Linq.Expressions;
using Core.Entities;

namespace Core.Specifications
{
    public class ProductWithBrandAndTypeSpecification:BaseSpecification<Product>
    {
        
        public ProductWithBrandAndTypeSpecification(ProductSpecParams param) :base(
            x=>
            (string.IsNullOrEmpty(param.Search) || x.Name.ToLower().Contains(param.Search.ToLower())) &&
            (!param.BrandId.HasValue || x.ProductBrandId== param.BrandId) &&
            (!param.TypeId.HasValue || x.ProductTypeId == param.TypeId))
        {
            AddInclude(x => x.ProductBrand);
            AddInclude(x => x.ProductType);

            //For Pagination
            ApplyPaging(param.PageSize * (param.PageIndex - 1), param.PageSize);

            //For sorting List
            if (!string.IsNullOrEmpty(param.Sort))
            {
                switch (param.Sort)
                {
                    case "priceAsc":
                        AddOrderBy(x => x.Price);
                        break;
                    case "priceDsc":
                        AddOrderByDescending(x => x.Price);
                        break;
                    case "nameDsc":
                        AddOrderByDescending(x => x.Name);
                        break;
                    default:
                        AddOrderBy(x => x.Name);
                        break;
                }
            }   
        }

        public ProductWithBrandAndTypeSpecification(int id) : base(x=>x.Id==id)
        {
            AddInclude(x => x.ProductBrand);
            AddInclude(x => x.ProductType);
        }

        public ProductWithBrandAndTypeSpecification(ProductSpecParams param,bool isForCount) : base(
            x =>
            (string.IsNullOrEmpty(param.Search) || x.Name.ToLower().Contains(param.Search.ToLower())) &&
            (!param.BrandId.HasValue || x.ProductBrandId == param.BrandId) &&
            (!param.TypeId.HasValue || x.ProductTypeId == param.TypeId))
        {
            
        }
    }
}