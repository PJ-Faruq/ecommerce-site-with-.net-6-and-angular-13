using Core.Entities;
using Core.Specifications;

namespace Core.Interfaces
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task<T> GetByIdAsync(int id);
        Task<List<T>> GetAllWithoutSpec();
        Task<T> GetEntityWithSpec(ISpecification<T> spec);
        Task<List<T>> GetAllWithSpec(ISpecification<T> spec);
        Task<int> GetTotalCount(ISpecification<T> spec);
    }
}