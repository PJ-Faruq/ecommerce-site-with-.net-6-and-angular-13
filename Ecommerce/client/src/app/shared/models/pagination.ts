import { IProduct } from './product';

export class IPagination {
  pageIndex: number;
  pageSize: number;
  totalCount: number = 0;
  data: IProduct[];
}
