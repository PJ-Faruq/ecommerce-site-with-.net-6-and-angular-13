export class ShopParams {
    brandId: number = 0;
    typeId: number = 0;
    pageSize: number = 8;
    pageIndex: number = 1;
    search: string = "";
}
