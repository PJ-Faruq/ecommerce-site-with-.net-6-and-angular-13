import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-paginng-header',
  templateUrl: './paging-header.component.html',
  styleUrls: ['./paging-header.component.scss']
})
export class PagingHeaderComponent implements OnInit {

  @Input() pageSize:number;
  @Input() pageIndex:number;
  @Input() totalCount:number;
  constructor() { }

  ngOnInit(): void {
  }

}
