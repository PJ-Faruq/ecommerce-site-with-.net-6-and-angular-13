import { HttpClient } from '@angular/common/http';
import { identifierName } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  Basket,
  IBasket,
  IBasketItem,
  IBasketTotal,
} from '../shared/models/basket';
import { IProduct } from '../shared/models/product';

@Injectable({
  providedIn: 'root',
})
export class BasketService {
  baseUrl = environment.apiUrl;
  private basketSource = new BehaviorSubject<IBasket | null>(null);
  private basketTotalSource = new BehaviorSubject<IBasketTotal | null>(null);

  basket$ = this.basketSource.asObservable();
  basketTotal$ = this.basketTotalSource.asObservable();

  constructor(private http: HttpClient) {}

  addOrUpdate(product: IProduct, quantity = 1) {
    const basketId = localStorage.getItem('basket_id');
    //If basketId has value the just UPDATE the basket
    if (basketId) {
      const basket = this.getCurrentBasket();
      //If already has basket with the id
      if (basket) {
        const basketItem: IBasketItem = this.convertProductToBasketItem(
          product,
          quantity
        );
        //Check if the basket has the same product
        const index = basket.items.findIndex((x) => x.id === basketItem.id);

        //if no product found then simply add an item to the Basket_Item_list
        if (index === -1) {
          basket.items.push(basketItem);
          this.setBasket(basket);
        }

        //if same product found then just update the quatity of the item
        else {
          basket.items[index].quantity =
            basket.items[index].quantity + basketItem.quantity;
          this.setBasket(basket);
        }
      }
    }
    // if no basketId is found then create a new basket and add the product/basketItem to the list
    else {
      const basket = new Basket();
      localStorage.setItem('basket_id', basket.id);
      const basketItem: IBasketItem = this.convertProductToBasketItem(
        product,
        quantity
      );
      basket.items.push(basketItem);
      this.setBasket(basket);
    }
  }

  convertProductToBasketItem(product: IProduct, quantity: number) {
    return {
      id: product.id,
      productName: product.name,
      price: product.price,
      quantity: quantity,
      pictureUrl: product.pictureUrl,
      brand: product.productBand,
      type: product.productType,
    };
  }

  getBusket(id: string) {
    return this.http.get<IBasket>(this.baseUrl + 'Baskets/' + id).pipe(
      map((basket: IBasket) => {
        this.basketSource.next(basket);
        this.calculateTotal();
      })
    );
  }

  setBasket(basket: IBasket) {
    this.http.post<IBasket>(this.baseUrl + 'baskets', basket).subscribe({
      next: (response: IBasket) => {
        this.basketSource.next(basket);
        this.calculateTotal();
      },
      error: (error) => {
        console.log(error);
      },
    });
  }

  calculateTotal() {
    const currentBasket = this.getCurrentBasket();
    const shipping = 0;
    const subtotal = currentBasket!.items.reduce(
      (a, b) => b.quantity * b.price + a,
      0
    );
    const total = shipping + subtotal;
    this.basketTotalSource.next({ shipping, subtotal, total });
  }

  getCurrentBasket() {
    return this.basketSource.value;
  }

  getBaketTotal() {
    return this.basketTotalSource.value;
  }

  decrementBasketItem(item: IBasketItem) {
    const currentBasket = this.getCurrentBasket();

    if (currentBasket?.items.some((m) => m.id === item.id)) {
      if (item.quantity > 1) {
        const index = currentBasket.items.indexOf(item);
        currentBasket.items[index].quantity--;
        this.setBasket(currentBasket);
      } else {
        this.removeItemFromBasket(item);
      }
    }
  }
  removeItemFromBasket(item: IBasketItem) {
    const currentBasket = this.getCurrentBasket();
    if (currentBasket?.items.some((m) => m.id === item.id)) {
      currentBasket.items = currentBasket.items.filter((m) => m.id != item.id);

      if (currentBasket.items.length > 0) {
        this.setBasket(currentBasket);
      } else {
        this.removeBasket(currentBasket);
      }
    }
  }
  removeBasket(basket: IBasket) {
    this.http.delete(this.baseUrl + 'baskets/' + basket.id).subscribe(
      () => {
        this.basketSource.next(null);
        this.basketTotalSource.next(null);
        localStorage.removeItem('basket_id');
      },
      (error) => {
        console.log(error);
      }
    );
  }
  incrementBasketItem(item: IBasketItem) {
    const currentBasket = this.getCurrentBasket();

    if (currentBasket?.items.some((m) => m.id === item.id)) {
      const index = currentBasket.items.indexOf(item);
      currentBasket.items[index].quantity++;
      this.setBasket(currentBasket);
    }
  }
}
