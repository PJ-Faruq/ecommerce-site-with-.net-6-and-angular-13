import { Component, OnInit } from '@angular/core';
import { map, Observable } from 'rxjs';
import { IBasket, IBasketItem } from '../shared/models/basket';
import { BasketService } from './basket.service';
import {
  faTrash,
  faCirclePlus,
  faCircleMinus,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss'],
})
export class BasketComponent implements OnInit {

  isBasketEmpty: boolean ;
  basket$: Observable<IBasket | null>;
  faCirclePlus = faCirclePlus;
  faCircleMinus = faCircleMinus;
  faTrash = faTrash;
  constructor(private basketService: BasketService) {}

  ngOnInit(): void {
    this.basket$ = this.basketService.basket$;
    this.checkBasketIsEmptyOrNot();
    
    
  }
  checkBasketIsEmptyOrNot() {
    this.basket$.subscribe((res) => {
      if (res === null) {
        this.isBasketEmpty = true;
      } else {
        this.isBasketEmpty = false;
      }
    });
  }

  onIncrement(item: IBasketItem) {
    this.basketService.incrementBasketItem(item);
  }
  onDecrement(item: IBasketItem) {
    this.basketService.decrementBasketItem(item);
  }

  onRemove(item: IBasketItem) {
    this.basketService.removeItemFromBasket(item);
  }
}
