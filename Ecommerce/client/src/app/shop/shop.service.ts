import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { IBrand } from '../shared/models/brand';
import { IPagination } from '../shared/models/pagination';
import { IProduct } from '../shared/models/product';
import { ShopParams } from '../shared/models/shopParams';
import { ITypes } from '../shared/models/type';

@Injectable({
  providedIn: 'root',
})
export class ShopService {
  BASE_URL = 'https://localhost:7077/api/';
  constructor(private http: HttpClient) {}

  getProducts(shopParam: ShopParams) {
    let params = new HttpParams();

    if (shopParam.brandId !== 0) {
      params = params.append('BrandId', shopParam.brandId.toString());
    }

    if (shopParam.typeId !== 0) {
      params = params.append("typeId", shopParam.typeId);
    }

    if (shopParam.search) {
      params = params.append('search', shopParam.search);
    }

    params = params.append("pageIndex", shopParam.pageIndex.toString());
    params = params.append("pageSize", shopParam.pageSize.toString());
    return this.http
      .get<IPagination>(this.BASE_URL + 'products', {
        observe: 'response',
        params,
      })
      .pipe(
        map((response) => {
          return response.body;
        })
      );
  }

  getProductById(id: number) {
    return this.http.get<IProduct>(this.BASE_URL + "products/" + id);
  }

  getBrands() {
    return this.http.get<IBrand[]>(this.BASE_URL + 'products/brands');
  }

  getTypes() {
    return this.http.get<ITypes[]>(this.BASE_URL + 'products/types');
  }
}
