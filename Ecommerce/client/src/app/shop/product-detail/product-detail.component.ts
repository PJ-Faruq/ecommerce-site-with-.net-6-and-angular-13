import { Component, OnInit } from '@angular/core';
import { IProduct } from 'src/app/shared/models/product';
import { BreadcrumbService } from 'xng-breadcrumb';
import { ShopService } from '../shop.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  product: IProduct;
  constructor(private shopService:ShopService,private bcService:BreadcrumbService) { }

  ngOnInit(): void {
    this.getProductById(2);
  }
  getProductById(id: number) {
    this.shopService.getProductById(2).subscribe(
      response => {
        this.product = response;
        this.bcService.set("@productDetail", this.product.name);
        
      }
      
    )
  }

  

}
