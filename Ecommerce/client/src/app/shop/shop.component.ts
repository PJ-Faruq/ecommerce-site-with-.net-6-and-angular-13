import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { IBrand } from '../shared/models/brand';
import { IPagination } from '../shared/models/pagination';
import { IProduct } from '../shared/models/product';
import { ShopParams } from '../shared/models/shopParams';
import { ITypes } from '../shared/models/type';
import { ShopService } from './shop.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss'],
})
export class ShopComponent implements OnInit {

  @ViewChild("search", { static: true }) searchInput:ElementRef;
  products: IProduct[];
  pagination = new IPagination();
  brands: IBrand[];
  types: ITypes[];
  selectedBrandId: number;
  params = new ShopParams();

  constructor(private shopService: ShopService) {
    
  }

  ngOnInit(): void {
    this.getBrandList();
    this.getTypeList();
    this.getProductList();
  }

  getProductList() {
    this.shopService.getProducts(this.params).subscribe((response) => {
      this.products = response!.data;
      this.pagination = response!;
     
    });
  }

  getBrandList() {
    this.shopService.getBrands().subscribe((response) => {
      this.brands = [{ name: 'All', id: 0 }, ...response];
    });
  }

  getTypeList() {
    this.shopService.getTypes().subscribe((response) => {
      this.types = [{id:0,name:"All"},...response];
    });
  }

  onBrandSelected(brandId: number) {
    this.params.brandId = brandId;
    this.getProductList();
  }

  onTypeSelected(typeId: number) {
    this.params.typeId = typeId;
    this.getProductList();
  }

  onPageChanged(pageValue: number) {
    this.params.pageIndex = pageValue;
    this.getProductList();
  }

  onSearch() {
    this.params.search = this.searchInput.nativeElement.value;
    this.getProductList();
  }

  onReset() {
    this.searchInput.nativeElement.value = undefined;
    this.params = new ShopParams();
    this.getProductList();
  }
}
