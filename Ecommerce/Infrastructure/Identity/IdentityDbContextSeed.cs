﻿using Core.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Identity
{
    public class IdentityDbContextSeed
    {
        public static void  SeedUser(UserManager<AppUser> userManager)
        {

            if (!userManager.Users.Any())
            {
                var user = new AppUser
                {
                    DisplayName = "PJ",
                    Email = "pj@test.com",
                    UserName = "pj@test.com",
                    Address = new Address
                    {
                        FirstName = "PJ",
                        LastName = "Faruq",
                        Street = "12/a",
                        City = "Dhaka",
                        State = "Dhaka",
                        ZipCode = "1207"
                    }

                };

                userManager.CreateAsync(user,"Pa$$w0rd");
            }

        }
    }
}
