﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public static class ModelBuilderExtension
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            
            try
            {
                modelBuilder.Entity<ProductType>().HasData(GetProductTypeList());
                modelBuilder.Entity<ProductBrand>().HasData(GetProductBandList());              
                modelBuilder.Entity<Product>().HasData(GetProductList());
            }
            catch (Exception e)
            {

                throw;
            }
        }

        private static IEnumerable<ProductBrand> GetProductBandList()
        {

            var jsonData = File.ReadAllText("../Infrastructure/Data/SeedDataFile/brands.json");
            var dataList = JsonSerializer.Deserialize<List<ProductBrand>>(jsonData);
            return dataList;
        }

        private static IEnumerable<ProductType> GetProductTypeList()
        {
            var jsonData = File.ReadAllText("../Infrastructure/Data/SeedDataFile/types.json");
            var dataList = JsonSerializer.Deserialize<List<ProductType>>(jsonData);
            return dataList;
        }

        private static IEnumerable<Product> GetProductList()
        {
            var jsonData = File.ReadAllText("../Infrastructure/Data/SeedDataFile/products.json");
            var dataList = JsonSerializer.Deserialize<List<Product>>(jsonData);
            var finalList = new List<Product>();
            int count = 1;
            foreach (var item in dataList)
            {
                
                Product product = new Product();
                product.Id = count++;               // Generating "Id" which was not given
                product.Name = item.Name;
                product.Description = item.Description;
                product.Price = item.Price;
                product.PictureUrl = item.PictureUrl;
                product.ProductTypeId = item.ProductTypeId;
                product.ProductBrandId = item.ProductBrandId;

                finalList.Add(product);

            }
            return finalList;
        }


    }
}
