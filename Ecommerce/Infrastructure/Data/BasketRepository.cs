﻿using Core.Entities;
using Core.Interfaces;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class BasketRepository : IBasketRepository
    {
        private readonly IDatabase _db;

        public BasketRepository(IConnectionMultiplexer multiplexer)
        {
            _db = multiplexer.GetDatabase();
        }
        public async Task<bool> DeleteBasketAsync(string id)
        {
           return await _db.KeyDeleteAsync(id);
        }

        public async Task<CustomerBasket> GetBasketAsync(string id)
        {
            var data =await _db.StringGetAsync(id);
            if (data.IsNullOrEmpty)
            {
                return null;
            }
            return JsonSerializer.Deserialize<CustomerBasket>(data);
        }

        public async Task<CustomerBasket> UpdateBasketAsync(CustomerBasket basket)
        {
            var created=await _db.StringSetAsync(basket.Id,JsonSerializer.Serialize(basket),TimeSpan.FromDays(30));
            if (!created)
            {
                return null;
            }

            return await GetBasketAsync(basket.Id);

        }
    }
}
