using API.Extensions;
using API.Helper;
using Core.Entities;
using Core.Interfaces;
using Infrastructure.Data;
using Infrastructure.Identity;
using Infrastructure.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StackExchange.Redis;

var builder = WebApplication.CreateBuilder(args);

var provider = builder.Services.BuildServiceProvider();
var _config=provider.GetRequiredService<IConfiguration>();



// Add services to the container.

//Dependency Injection for Jwt Token Services
builder.Services.AddScoped<ITokenService, TokenService>();

builder.Services.AddControllers();

//SqLite database configuration
builder.Services.AddDbContext<StoreDbContext>(x => x.UseSqlite(builder.Configuration.GetConnectionString("DefaultConnection")));

//SqLite database configuration for Identity
builder.Services.AddDbContext<AppIdentityDbContext>(x => x.UseSqlite(builder.Configuration.GetConnectionString("IdentityConnection")));

//This is a Extension Method for Identity related services(Jwt configuration, Seeding Data)
builder.Services.AddIdentityServices(_config);


//Redis Configuration
builder.Services.AddSingleton<IConnectionMultiplexer>(c =>
{
    var configuration = ConfigurationOptions.Parse(builder.Configuration.GetConnectionString("Redis"), true);
    return ConnectionMultiplexer.Connect(configuration);
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAutoMapper(typeof(MappingProfiles));




builder.Services.AddScoped<IProductRepository,ProductRepository>();
builder.Services.AddScoped(typeof(IGenericRepository<>),typeof(GenericRepository<>));
builder.Services.AddScoped<IBasketRepository, BasketRepository>();




//Cors Policy-> Then add this to Service
builder.Services.AddCors(opt =>
{
    opt.AddPolicy("CorsPolicy", policy =>
    {
        policy.AllowAnyHeader().AllowAnyMethod().WithOrigins("https://localhost:4200");
    });
});



var app = builder.Build();


// For Seeding Data
using(var scope = app.Services.CreateScope())
{
    var services= scope.ServiceProvider;
    var userManager = services.GetRequiredService<UserManager<AppUser>>();
    IdentityDbContextSeed.SeedUser(userManager);

}


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseCors("CorsPolicy");
app.UseStaticFiles();  //For Serverable folder inside wwwroot 
app.UseAuthentication();
app.UseAuthorization();


app.MapControllers();


app.Run();
