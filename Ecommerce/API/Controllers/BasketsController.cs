﻿using API.Dtos;
using AutoMapper;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BasketsController : ControllerBase
    {
        private readonly IBasketRepository _repo;
        private readonly IMapper _mapper;

        public BasketsController(IBasketRepository repo,IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerBasket>> GetBasketAsync(string id)
        {
            var basket= await _repo.GetBasketAsync(id);
            return (basket ?? new CustomerBasket(id));
        }

        [HttpPost]
        public async Task<ActionResult<CustomerBasket>> UpdateBasketAsync(CustomerBasketDto customerBasketDto)
        {
            
            return  await _repo.UpdateBasketAsync(_mapper.Map<CustomerBasketDto,CustomerBasket>(customerBasketDto));
            
        }

        [HttpDelete("{id}")]
        public async Task DeleteBasketAsync(string id)
        {
            await _repo.DeleteBasketAsync(id);
            
        }

    }
}
