using API.Dtos;
using API.Helper;
using AutoMapper;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/products")]
    public class ProductsController:ControllerBase
    {
        private readonly IGenericRepository<Product> _productRepo;
        private readonly IGenericRepository<ProductBrand> _brandRepo;
        private readonly IGenericRepository<ProductType> _typeRepo;
        private readonly IMapper _mapper;

        public ProductsController(IGenericRepository<Product> productRepo,IGenericRepository<ProductBrand> brandRepo,IGenericRepository<ProductType> typeRepo, IMapper mapper)
        {
            _mapper = mapper;
            _productRepo = productRepo;
            _brandRepo = brandRepo;
            _typeRepo = typeRepo;
        }
        [HttpGet("{id}")]
        public async Task<ProductDto> GetProductAsync(int id)
        {
            var spec = new ProductWithBrandAndTypeSpecification(id);
            var product = await _productRepo.GetEntityWithSpec(spec);
            return _mapper.Map<Product,ProductDto>(product);
        }

        [HttpGet]
        public async Task<Pagination<ProductDto>> GetAllAsync([FromQuery] ProductSpecParams param)
        {
            var spec = new ProductWithBrandAndTypeSpecification(param);
            var productList = await _productRepo.GetAllWithSpec(spec);
            var data= _mapper.Map<List<Product>, List<ProductDto>>(productList);

            var countSpec=new ProductWithBrandAndTypeSpecification(param,true);
            int totalCount = await _productRepo.GetTotalCount(countSpec);

            var result = new Pagination<ProductDto>(param.PageIndex,param.PageSize,totalCount,data);
            return result;

        }

        [HttpGet("brands")]
        public async Task<ActionResult<IEnumerable<ProductBrand>>> GetBrands()
        {
            var brandList = await _brandRepo.GetAllWithoutSpec();
            return  Ok(brandList);

        }

        [HttpGet("types")]
        public async Task<ActionResult<IEnumerable<ProductType>>> GetTypes()
        {
            var typeList = await _typeRepo.GetAllWithoutSpec();
            return Ok(typeList);

        }
    }
}